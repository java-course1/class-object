public class Student {
    private int id;
    private String name;
    private String gender;

    // read only
    public String getName() {
        if (gender.equals("male")) {
            return name;
        }
        return null;
    }

    // write only
    public void setName(String name) {
        if (!name.equals(null)) {
            this.name = name;
        }
    }
}
