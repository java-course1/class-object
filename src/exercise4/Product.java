package exercise4;

public class Product extends Object {
    private int proCode;
    private String name;
    private double price;
    private String madeIn;
    private int amount;

    public Product(){}

    public Product(int proCode,
                   String name,
                   double price,
                   String madeIn,
                   int amount) {

        this.proCode = proCode;
        this.name = name;
        this.price = price;
        this.madeIn = madeIn;
        this.amount = amount;
    }

    // check stock of product
    public int getAmount() {
        return amount;
    }

    // find products by made in
    public String findProductByMadeIn (String madeIn) {
        if (this.madeIn.equals(madeIn)) {
            return "Product: code: " + proCode +
                    " , name: " + name +
                    " , price: " + price +
                    " , made in: " + madeIn + " , amount: " + amount;
        } else {
            return "";
        }
    }

    // find product by code
    public String findProductByCode (int code) {
        if (proCode == code) {
            return "Product: code: " + proCode +
                    " , name: " + name +
                    " , price: " + price +
                    " , made in: " + madeIn + " , amount: " + amount;
        } else {
            return "";
        }
    }

    @Override
    public String toString() {
        return "Product{" +
                "proCode=" + proCode +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", madeIn='" + madeIn + '\'' +
                ", amount=" + amount +
                '}';
    }
}
