package exercise1;

public class Main {

    public static void main(String[] args) {
        Room room = new Room();
        Room[] rooms = {
                new Room(1, "Single", true, 12.90),
                new Room(2, "Double", false, 2.3), // Room (2, "Triple, true, 2.3)
                new Room(3, "Triple", false, 4.5),
                new Room(4, "Single", true    , 5.4),
                new Room(),
        };

        for (Room room1: rooms) {
//                int numOfBeds = room1.getBedsOfRoomType("Double");
//                System.out.println(numOfBeds);

            if (room1.getRoomId() == 2) {
                room1.setRoomType("Triple");
            }
            System.out.println(room1.toString());
        }
    }
}
