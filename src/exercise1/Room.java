package exercise1;

public class Room {
    private int roomId;
    private String roomType;
    private boolean roomStatus;
    private double price;

    public Room () {
        this.roomId = 1;
        this.roomType = "Single";
        this.roomStatus = true;
        this.price = 0.0;
    }
    public Room(int roomId, String roomType, boolean roomStatus, double price) {
        this.roomId = roomId;
        this.roomType = roomType;
        this.roomStatus = roomStatus;
        this.price = price;
    }

    public int getRoomId () {
        return roomId;
    }
    public boolean getStatus () {
        return roomStatus;
    }

    public String[] getRoomType () {
        return new String[] {"Single", "Double", "Triple"};
    }

    public int getBedsOfRoomType (String roomType) {
            if (roomType.equals("Single")) {
                return 1;
            } else if (roomType.equals("Double")) {
                return 2;
            } else if (roomType.equals("Triple")) {
                return 3;
            }

        return 0;
    }

    public double getPrice () {
        return price;
    }

    public void setRoomType (String roomType) {
        this.roomType = roomType;
    }

    @Override
    public String toString() {
        return "Room{" +
                "roomId=" + roomId +
                ", roomType=" + roomType +
                ", roomStatus=" + roomStatus +
                ", price=" + price +
                '}';
    }
}
