package inheritance;

public class Student extends Person {
    // Person is Super class && Student is Sub class

    private double score;

    public Student() {
    }

    public Student(double score) {
        this.score = score;
    }

    public Student(int id, String name, String gender, double score) {
        super(id, name, gender); // super();
        this.score = score;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Student{" +
                "score=" + score +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }
}
