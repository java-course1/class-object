package nestedClass;

public class Main extends AbstractClass {
    public static void main(String[] args) {
        Main main = new Main();
        main.message();
    }

    @Override
    void message() {
        System.out.println("Hello Abstract");
    }
}
