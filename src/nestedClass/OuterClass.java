package nestedClass;

public class OuterClass{
    public static void main(String[] args) {
        AbstractClass abstractClass = new AbstractClass() {
            @Override
            void message() {
                System.out.println("Hello World");
            }
        };
        abstractClass.message();

    }

}
